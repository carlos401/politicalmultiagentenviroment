package project.agents;

import jade.core.Agent;
import project.behaviours.InitialPoliticianBehaviour;
import project.behaviours.PoliticianBehaviour;

/**
 *
 */
public class Politician extends Agent {

    /**
     *
     */
    private double persuasion;

    /**
     *
     */
    private double leadership;

    /**
     *
     */
    private double money;

    /**
     *
     */
    public Politician(){
        super();
        this.persuasion = Math.max(Math.random() * 98 + 1,65); // 1 a 99
        this.leadership = Math.max(Math.random() * 98 + 1,65); // 1 a 99
        this.money = Math.max(Math.random() * 98 + 1,60); // 1 a 99
    }

    /**
     *
     */
    protected void setup() {
        addBehaviour(new InitialPoliticianBehaviour(this));
        addBehaviour(new PoliticianBehaviour(this));
    }

    /**
     *
     * @return
     */
    public double getPersuasion() {
        return persuasion;
    }

    /**
     *
     * @return
     */
    public double getLeadership() {
        return leadership;
    }

    /**
     *
     * @return
     */
    public double getMoney() {
        return money;
    }
}
