package project.agents;

import jade.core.Agent;
import project.behaviours.InitialPrinterBehaviour;
import project.behaviours.PrinterBehaviour;
import project.gui.GraphicalUserInterface;

/**
 *
 */
public class Printer extends Agent {

    private GraphicalUserInterface graphicalUserInterface;

    public Printer(){
        super();
        this.graphicalUserInterface = new GraphicalUserInterface();
    }

    /**
     *
     */
    protected void setup() {
        addBehaviour(new InitialPrinterBehaviour(this));
        addBehaviour(new PrinterBehaviour(this ));
    }

    /**
     *
     * @return
     */
    public GraphicalUserInterface getGraphicalUserInterface() {
        return graphicalUserInterface;
    }
}
