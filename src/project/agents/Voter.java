package project.agents;

import jade.core.Agent;
import project.behaviours.InitialVoterBehaviour;
import project.behaviours.VoterBehaviour;

/**
 *
 */
public class Voter extends Agent {

    /**
     *
     */
    private String candidate;

    /**
     *
     */
    private double fidelity;

    /**
     *
     */
    public Voter(){
        super();
        this.candidate = "none";
        this.fidelity = Math.random() * 20 + 1;
    }

    /**
     *
     */
    protected void setup()
    {
        addBehaviour(new InitialVoterBehaviour(this));
        addBehaviour(new VoterBehaviour(this));
    }

    /**
     *
     * @return
     */
    public String getCandidate() {
        return candidate;
    }

    /**
     *
     * @param candidate
     */
    public void setCandidate(String candidate) {
        this.candidate = candidate;
    }

    /**
     *
     * @return
     */
    public double getFidelity() {
        return fidelity;
    }
}
