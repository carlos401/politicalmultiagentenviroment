package project.gui;
import project.statistics.Controller;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;
import java.util.Random;
import javax.swing.*;

import static java.lang.Thread.sleep;

public class GraphicalUserInterface extends JFrame {

    //private JTextField txtURL;
    private JProgressBar pbPartidoCero, pbPartidoUno, pbPartidoDos, pbPartidoTres;
    private JButton btnDownload;
    private Map<String, Integer> data;
    private int percentaje;

    public void StartGraphical() {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                GraphicalUserInterface form = new GraphicalUserInterface();
                form.setVisible(true);
            }
        });
    }

    public GraphicalUserInterface() {
        // Create Form Frame
        super("Multi Agents System");
        setSize(850, 450);
        setLocation(350, 100);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);
        getContentPane().setBackground(Color.LIGHT_GRAY);

        // valores de tamaño para las barras de progreso
        int pbx, pby, pbDistancy, pbWwidth, pbHeight;

        // etiquetas de la gui
        final JLabel lblTitle, peligroTitle, signalP, amenazaTitle, signalA, dominanteTitle, signalD;
        final JLabel pCeroEtq, pUnoEtq, pDosEtq, pTresEtq;

        int pbMin, pbMax;

        // Label Title
        lblTitle = new JLabel("Sistema Politico", JLabel.CENTER);
        lblTitle.setBounds(0, 20, 370, 40);
        lblTitle.setFont(new Font("Console", Font.BOLD, 30));
        getContentPane().add(lblTitle);

        peligroTitle = new JLabel("Iniciativas Peligrosas", JLabel.CENTER);
        peligroTitle.setBounds(200, 60, 370, 14);
        getContentPane().add(peligroTitle);
        signalP = new JLabel("|", JLabel.CENTER);
        signalP.setBounds(205, 75, 370, 14);
        getContentPane().add(signalP);

        amenazaTitle = new JLabel("Amenaza", JLabel.CENTER);
        amenazaTitle.setBounds(400, 60, 370, 14);
        getContentPane().add(amenazaTitle);
        signalA = new JLabel("|", JLabel.CENTER);
        signalA.setBounds(401, 75, 370, 14);
        getContentPane().add(signalA);

        dominanteTitle = new JLabel("Dominante", JLabel.CENTER);
        dominanteTitle.setBounds(527, 60, 370, 14);
        getContentPane().add(dominanteTitle);
        signalD = new JLabel("|", JLabel.CENTER);
        signalD.setBounds(528, 75, 370, 14);
        getContentPane().add(signalD);

        // minimo y maximo de las barras de progreso
        pbMin = 0;
        pbMax = 100;

        // values for positioning
        pbx = 55;
        pby = 90;
        pbDistancy = 70;
        pbWwidth = 730;
        pbHeight = 50;

        pCeroEtq = new JLabel("P1", JLabel.CENTER);
        pCeroEtq.setBounds(pbx-40, pby+15, 30, 14);
        pCeroEtq.setFont(new Font("Console", Font.BOLD, 15));
        getContentPane().add(pCeroEtq);

        pUnoEtq = new JLabel("P2", JLabel.CENTER);
        pUnoEtq.setBounds(pbx-40, pby+pbDistancy*1+15, 30, 14);
        pUnoEtq.setFont(new Font("Console", Font.BOLD, 15));
        getContentPane().add(pUnoEtq);

        pDosEtq = new JLabel("P3", JLabel.CENTER);
        pDosEtq.setBounds(pbx-40, pby+pbDistancy*2+15, 30, 14);
        pDosEtq.setFont(new Font("Console", Font.BOLD, 15));
        getContentPane().add(pDosEtq);

        pTresEtq = new JLabel("P4", JLabel.CENTER);
        pTresEtq.setBounds(pbx-40, pby+pbDistancy*3+15, 30, 14);
        pTresEtq.setFont(new Font("Console", Font.BOLD, 15));
        getContentPane().add(pTresEtq);

        UIManager.put("ProgressBar.selectionForeground", Color.BLACK);

        // pbPartidoCero
        pbPartidoCero = new JProgressBar();
        pbPartidoCero.setStringPainted(true);
        pbPartidoCero.setMinimum(0);
        pbPartidoCero.setMaximum(100);
        pbPartidoCero.setBounds(pbx, pby, pbWwidth, pbHeight);
        pbPartidoCero.setForeground(Color.RED);
        getContentPane().add(pbPartidoCero);

        pbPartidoUno = new JProgressBar();
        pbPartidoUno.setStringPainted(true);
        pbPartidoUno.setMinimum(pbMin);
        pbPartidoUno.setMaximum(pbMax);
        pbPartidoUno.setBounds(pbx, pby+pbDistancy, pbWwidth, pbHeight);
        pbPartidoUno.setForeground(Color.darkGray);
        getContentPane().add(pbPartidoUno);

        pbPartidoDos = new JProgressBar();
        pbPartidoDos.setStringPainted(true);
        pbPartidoDos.setMinimum(pbMin);
        pbPartidoDos.setMaximum(pbMax);
        pbPartidoDos.setBounds(pbx, pby+pbDistancy*2, pbWwidth, pbHeight);
        pbPartidoDos.setForeground(Color.GREEN);
        getContentPane().add(pbPartidoDos);

        pbPartidoTres = new JProgressBar();
        pbPartidoTres.setStringPainted(true);
        pbPartidoTres.setMinimum(pbMin);
        pbPartidoTres.setMaximum(pbMax);
        pbPartidoTres.setBounds(pbx, pby+pbDistancy*3, pbWwidth, pbHeight);
        pbPartidoTres.setForeground(Color.YELLOW);
        getContentPane().add(pbPartidoTres);

        // Button Download
        btnDownload = new JButton("Actualizar");
        btnDownload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnDownload.setEnabled(false);
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        new BackgroundWorker().execute();
                    }
                });

            }
        });
        btnDownload.setBounds(350, pby+pbDistancy*4, 100, 23);
        getContentPane().add(btnDownload);

    }

    public class BackgroundWorker extends SwingWorker<Void, Void> {

        public BackgroundWorker() {
            addPropertyChangeListener(new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    //System.out.println("Cantidad de votantes" + Controller.getTotalOfVoters());
                    if(Controller.getTotalOfVoters() != 0){
                        percentaje = 100 / Controller.getTotalOfVoters();
                    }
                    else{
                        percentaje = 0;
                    }
                    try{
                        if(data.get("P1") != null){
                            pbPartidoCero.setValue(data.get("P1") * percentaje);
                        }
                        else{
                            pbPartidoCero.setValue(0);
                        }
                        if(data.get("P2") != null){
                            pbPartidoUno.setValue(data.get("P2") * percentaje);
                        }
                        else{
                            pbPartidoUno.setValue(0);
                        }
                        if(data.get("P3") != null){
                            pbPartidoDos.setValue(data.get("P3") * percentaje);
                        }
                        else{

                        }
                        if(data.get("P4") != null){
                            pbPartidoTres.setValue(data.get("P4") * percentaje);
                        }
                        else{
                            pbPartidoTres.setValue(0);
                        }
                    }
                    catch (Exception e){
                        //e.printStackTrace();
                    }
                }

            });
        }

        @Override
        protected void done() {
            btnDownload.setEnabled(true);
        }

        /*
         * Main task. Executed in background thread.
         */
        @Override
        public Void doInBackground() {
            Random random = new Random();
            int progress = 0;
            //Initialize progress property.
            setProgress(0);
            while (progress < 100) {
                //Sleep for up to one second.
                try {
                    sleep(random.nextInt(1000));
                } catch (InterruptedException ignore) {}
                //Make random progress.
                progress += random.nextInt(10);
                setProgress(Math.min(progress, 100));
            }
            data = Controller.getStatistics();

            return null;
        }
    }

}
