package project.statistics;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class Controller {

    /**
     *
     */
    public static int totalOfVoters;

    /**
     *
     */
    public static Map<String,Integer> statistics;

    /**
     *
     * @param winner
     * @param loser
     */
    public static void updateStatistics(String winner, String loser){
        if(statistics==null){
            statistics = new HashMap<>();
        }
        // increases the followers of the winner
        statistics.put(winner,
                statistics.get(winner)+1);

        if(!loser.equals("none")){
            // decreases the followers of the loser
            int followersOfLoser = statistics.get(loser);
            if(followersOfLoser>0){
                statistics.put(loser,
                        statistics.get(loser)-1);
            }
        }
    }

    /**
     *
     */
    public static void registerVoter(){
        totalOfVoters++;
    }

    /**
     *
     * @param name
     */
    public static void registerPolitician(String name){
        if(statistics==null){
            statistics = new HashMap<>();
        }
        statistics.put(name,0);
    }

    /**
     *
     * @return
     */
    public static int getTotalOfVoters() {
        return totalOfVoters;
    }

    /**
     *
     * @return
     */
    public static Map<String, Integer> getStatistics() {
        return statistics;
    }
}
