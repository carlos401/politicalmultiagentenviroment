package project.behaviours;

import jade.core.behaviours.SimpleBehaviour;
import project.agents.Politician;
import project.statistics.Controller;

/**
 *
 */
public class InitialPoliticianBehaviour extends SimpleBehaviour {

    /**
     *
     */
    private Politician agent;

    /**
     *
     * @param agent
     */
    public InitialPoliticianBehaviour(Politician agent){
        super(agent);
        this.agent = agent;
    }

    /**
     *
     */
    public void action() {
        Controller.registerPolitician(this.agent.getLocalName()); // registers the new politician
        System.out.println("-- NEW POLITICIAN DESCRIPTION --");
        System.out.println("* NAME: \t\t" + this.agent.getLocalName());
        System.out.println("* MONEY: \t\t" + this.agent.getMoney());
        System.out.println("* PERSUASION: \t" + this.agent.getPersuasion());
        System.out.println("* LEADERSHIP: \t" + this.agent.getLeadership());
        System.out.println();
    }

    /**
     *
     * @return
     */
    public boolean done() {
        return true;
    }
}
