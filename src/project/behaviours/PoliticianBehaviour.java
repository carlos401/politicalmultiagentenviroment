package project.behaviours;

import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import project.agents.Politician;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.*;
import project.protocol.Proposal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 */
public class PoliticianBehaviour extends CyclicBehaviour {

    /**
     *
     */
    private Politician agent;

    /**
     *
     */
    private AMSAgentDescription[] agents = null;

    /**
     *
     * @param a
     */
    public PoliticianBehaviour(Politician a){
        super(a);
        agent = a;
    }

    /**
     *
     */
    public void action() {
        refreshVoters();
        doPoliticalCampaign();
        try{
            Thread.sleep(9000);
        } catch (Exception e){
            System.out.println("The Thread was interrupted");
            //e.printStackTrace();
        }
    }

    /**
     *
     */
    private void refreshVoters(){
        try {
            SearchConstraints c = new SearchConstraints();
            c.setMaxResults (new Long(-1));
            this.agents = AMSService.search(agent, new AMSAgentDescription(),c);
        }
        catch (Exception e) {
            System.out.println( "Problem searching AMS: " + e );
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void doPoliticalCampaign(){
        try{
            AID myID = agent.getAID();
            //System.out.println("Political campaign target for " + this.agent.getLocalName() + ":");

            // shuffle array to list
            List<AMSAgentDescription> targetList = Arrays.asList(agents);
            Collections.shuffle(targetList); //randomize the order

            int sizeOfCampaign = targetList.size()/2; // reduces the number of targets

            for (int i=0; i < sizeOfCampaign; ++i) {
                AID agentID = targetList.get(i).getName();
                //System.out.println("\t -" + agentID.getLocalName());
                // create the proposal
                ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                msg.setContent(
                        Proposal.getProposal(this.agent)
                );
                msg.addReceiver(agentID);
                this.agent.send(msg);
            }
        } catch (Exception e){
            System.out.println( "Problem trying to do the political campaign " + e);
            e.printStackTrace();
        }
    }
}