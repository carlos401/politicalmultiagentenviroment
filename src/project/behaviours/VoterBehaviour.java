package project.behaviours;

import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import project.agents.Voter;
import project.protocol.Proposal;
import project.statistics.Controller;
import java.util.List;

/**
 *
 */
public class VoterBehaviour extends CyclicBehaviour {

    /**
     *
     */
    private Voter agent;

    /**
     *
     * @param agent
     */
    public VoterBehaviour(Voter agent){
        super(agent);
        this.agent = agent;
    }

    /**
     *
     */
    public void action() {
        ACLMessage msg = agent.receive();
        if (msg!=null) {
            //System.out.println( " - " +
            //        myAgent.getLocalName() + " received: " +
            //        msg.getContent() );
            if( !this.agent.getCandidate().equals(msg.getSender().getLocalName()) &&
                    imConvinced(msg.getContent())){
                System.out.println(
                        this.agent.getLocalName() + " was convinced by: " + msg.getSender().getLocalName());
                String myNewCandidate = msg.getSender().getLocalName();
                Controller.updateStatistics(
                        myNewCandidate,this.agent.getCandidate());
                this.agent.setCandidate(myNewCandidate);
            }
        }
    }

    /**
     *
     * @param proposal
     * @return
     */
    private boolean imConvinced(String proposal){
        List<Double> profile = Proposal.castProfile(proposal);
        double persuasion = profile.get(0);
        double leadership = profile.get(1);
        double money = profile.get(2);
        return (
                persuasion * 0.45 +
                        leadership * 0.3 +
                        money * 0.25 -
                        this.agent.getFidelity())
                > 65;
    }
}
