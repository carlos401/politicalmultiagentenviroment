package project.behaviours;

import jade.core.behaviours.SimpleBehaviour;
import project.agents.Printer;
import project.gui.GraphicalUserInterface;

/**
 *
 */
public class InitialPrinterBehaviour extends SimpleBehaviour {

    /**
     *
     */
    private GraphicalUserInterface graphicalUserInterface;

    /**
     *
     * @param agent
     */
    public InitialPrinterBehaviour(Printer agent){
        this.graphicalUserInterface = agent.getGraphicalUserInterface();
    }

    /**
     *
     */
    public void action() {
        //initializing the Graphical User Interface
        graphicalUserInterface.StartGraphical();
    }

    /**
     *
     * @return
     */
    public boolean done() {
        return true;
    }
}
