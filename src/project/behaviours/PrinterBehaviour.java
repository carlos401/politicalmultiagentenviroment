package project.behaviours;

import jade.core.behaviours.CyclicBehaviour;
import project.agents.Printer;
import project.gui.GraphicalUserInterface;
import project.statistics.Controller;

/**
 *
 */
public class PrinterBehaviour extends CyclicBehaviour {

    /**
     *
     */
    private Printer printer;

    /**
     *
     * @param agent
     */
    public PrinterBehaviour(Printer agent){
        super(agent);
        this.printer = agent;
    }

    /**
     *
     */
    public void action() {
        /* Agregar actualización de gui **/
        System.out.println("Total of voters: " + Controller.getTotalOfVoters());
        System.out.println("Statistics: \n" + Controller.getStatistics());
        System.out.println();

        try{
            Thread.sleep(15000);
        } catch (Exception e){
            e.printStackTrace();
        }
    }


}
