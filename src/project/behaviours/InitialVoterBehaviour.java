package project.behaviours;

import jade.core.behaviours.SimpleBehaviour;
import project.agents.Voter;
import project.statistics.Controller;

/**
 *
 */
public class InitialVoterBehaviour extends SimpleBehaviour {

    /**
     *
     */
    private Voter agent;

    /**
     *
     * @param agent
     */
    public InitialVoterBehaviour(Voter agent){
        super(agent);
        this.agent = agent;
    }

    /**
     *
     */
    public void action() {
        Controller.registerVoter(); // registers the new voter
        System.out.println("-- NEW VOTER DESCRIPTION --");
        System.out.println("* NAME: \t\t" + this.agent.getLocalName());
        System.out.println("* FIDELITY: \t" + this.agent.getFidelity());
        System.out.println();
    }

    /**
     *
     * @return
     */
    public boolean done() {
        return true;
    }
}
