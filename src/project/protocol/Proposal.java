package project.protocol;

import project.agents.Politician;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 *
 */
public class Proposal {

    /**
     *
     * @param politician
     * @return
     */
    public static String getProposal(Politician politician){
        if (politician!=null){
            return  "" + politician.getLocalName() +
                    ";" + politician.getPersuasion() +
                    ";" + politician.getLeadership() +
                    ";" + politician.getMoney();
        }
        return null;
    }

    /**
     *
     * @param proposal
     * @return
     */
    public static List<Double> castProfile(String proposal){
        StringTokenizer st = new StringTokenizer(proposal,";",false);
        st.nextToken(); // this is the name
        List<Double> answer = new ArrayList<>(3);
        while(st.hasMoreTokens()){
            answer.add(Double.parseDouble(st.nextToken()));
        }
        return answer;
    }

    /**
     *
     * @param proposal
     * @return
     */
    public static String castSender (String proposal){
        StringTokenizer st = new StringTokenizer(proposal,";",false);
        return st.nextToken(); // this is the name
    }
}
