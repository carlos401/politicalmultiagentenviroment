package examples.chatingAgents;

import jade.core.Agent;
import jade.core.behaviours.*;
import jade.lang.acl.*;

public class ReceiverBehaviour extends SimpleBehaviour {

    private Agent agent;

    private boolean finished;

    public ReceiverBehaviour(Agent a) {
        super(a);
        this.agent = a;
        this.finished = false;
    }

    @Override
    public void action() {
        ACLMessage msg = agent.receive();
        if (msg!=null) {
            System.out.println( " - " +
                    myAgent.getLocalName() + " received: " +
                    msg.getContent() );

            ACLMessage reply = msg.createReply();
            reply.setPerformative( ACLMessage.INFORM );
            reply.setContent("Hola! Mi nombre es " + agent.getLocalName() );
            agent.send(reply);
        }
        block();
    }

    @Override
    public boolean done() {
        return false;
    }
}
