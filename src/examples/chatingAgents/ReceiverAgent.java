package examples.chatingAgents;

import jade.core.Agent;

public class ReceiverAgent extends Agent {
    @Override
    protected void setup() {
        addBehaviour(new ReceiverBehaviour(this));
    }
}
