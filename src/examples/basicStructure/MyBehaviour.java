package examples.basicStructure;

import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;

public class MyBehaviour extends SimpleBehaviour
{
    private boolean finished = false;

    private Agent agent;

    public MyBehaviour(Agent a) {
        super(a);
        this.agent = a;
    }

    public void action()
    {
        System.out.println("Hello! I'm " + agent.getLocalName());
        finished = true;
    }

    public boolean done() {
        return finished;
    }

} // ----------- End myBehaviour
