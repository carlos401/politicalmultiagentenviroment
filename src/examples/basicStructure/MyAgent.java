package examples.basicStructure;

import jade.core.Agent;

public class MyAgent extends Agent
{
    protected void setup()
    {
        addBehaviour( new MyBehaviour( this ) );
    }
}//end class myAgent
