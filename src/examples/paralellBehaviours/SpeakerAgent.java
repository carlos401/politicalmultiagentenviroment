package examples.paralellBehaviours;

import jade.core.Agent;

public class SpeakerAgent extends Agent
{
    protected void setup()
    {
        addBehaviour( new SpeakerBehaviour( this,5000 ) );
    }
}
