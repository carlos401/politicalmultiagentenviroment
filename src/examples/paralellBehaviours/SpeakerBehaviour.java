package examples.paralellBehaviours;

import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;

public class SpeakerBehaviour extends SimpleBehaviour {
    static String offset = "";
    static long   t0     = System.currentTimeMillis();

    String tab = "" ;
    int    state   = 0;
    long   dt;

    Agent agent;

    public SpeakerBehaviour(Agent a, long dt) {
        super(a);
        agent = a;
        this.dt = dt;
        offset += "    " ;
        tab = new String(offset) ;
    }

    public void action()
    {
        switch (state){
            case 0:
                System.out.println(
                        (System.currentTimeMillis()-t0)/10*10 + ": " +
                                "Pienso que la historia de la humanidad es una patraña" );
                block( dt );
                break;
            case 1:
                System.out.println(
                        (System.currentTimeMillis()-t0)/10*10 + ": " +
                                "Pienso que la historia de la humanidad no es una patraña" );
                block( dt );
                break;
            case 3:
                System.out.println(
                        (System.currentTimeMillis()-t0)/10*10 + ": " +
                                "No pude decidirme, me muero.." );
                agent.doDelete();
                break;
        }
        state++;
    }

    public  boolean done() {  return state>3;  }
}
