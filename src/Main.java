import project.gui.GraphicalUserInterface;

public class Main {
    public static void main(String[] args) {
        GraphicalUserInterface graphicalUserInterface = new GraphicalUserInterface();
        graphicalUserInterface.StartGraphical();
        System.out.println("\nMain\n");
    }
}

